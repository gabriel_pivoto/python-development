
print("---------------------- ex 1 -----------------------------------")

try: 
    num1 = int(input("Insira um numero inteiro: "))
    num2 = int(input("Insira o segundo numero inteiro: "))
    print(f"Todos os numero entre {num1} e {num2} são: ")
    for numEntre in range(num1+1,(num2)):
        print(numEntre)
except:
    print("O número inserido não foi inteiro ;(")


print("---------------------- ex 2 -----------------------------------")
try: 
    num1 = int(input("Insira um numero inteiro: "))
    print(f"A tabuada de {num1}: ")
    for x in range(0,11):
        print(f"{num1} * {x} = {num1* x}")
except:
    print("O número inserido não foi inteiro ;(")


