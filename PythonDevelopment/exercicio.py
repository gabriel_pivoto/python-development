print("--------------- ex 1 ------------------")
num1 = int(input("Insira o primeiro numero: "))
num2 = int(input("Insira o segundo numero: "))
if num1 > num2:
    print(f"Numero {num1} é o maior")
elif num2> num1:
    print(f"Numero {num2} é o maior")
else: 
    print("Os numeros são iguais")

print("--------------- ex 2 ------------------")
try: 
    perc1 = float(input("Insira o percentual de crescimento da sua empresa: "))
    if perc1 > 0.0:
        print(f"Houve um crescimento positivo")
    elif perc1 < 0.0:
        print(f"Houve um decrescimento da empresa")
    else: 
        print("A empresa ficou estável")
except:
    print("Insira somente o valor, sem %")


